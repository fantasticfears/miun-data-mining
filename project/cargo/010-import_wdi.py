from sqlalchemy import create_engine
import pandas as pd

engine = create_engine('postgresql://postgres@localhost/dm')
df = pd.read_csv('~/Downloads/WDI_csv/WDI_Data.csv')
df.to_sql('wdi', engine, if_exists='replace')
