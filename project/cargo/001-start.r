library(RWeka)

this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

d <- read.csv2('00-goods-type-cargo-handled-k-tons.csv')
print("== Summary ============================================")
summary(d)

df = subset(d, select=-c(year))
print(df)

oner <- OneR(GDP ~ ., data = df, control = Weka_control(B = 2))
oner_e <- evaluate_Weka_classifier(oner, seed = 1, class = TRUE)
print("== Result: OneR ============================================")
print(oner_e)

j48 <- J48(`GDP` ~ ., data = df, control = Weka_control(C = 0.25, M = 2))
j48_e <- evaluate_Weka_classifier(j48, seed = 1, class = TRUE, complexity = TRUE, numFolds = 2)
print("== Result: J48 ============================================")
summary(j48)
print(j48_e)
if(require("party", quietly = TRUE)) plot(j48)
write_to_dot(j48)
