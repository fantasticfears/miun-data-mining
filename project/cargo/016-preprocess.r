library(RWeka)
library(discretization)
library(mice)
library(VIM)

this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

d <- read.csv('017-diff-google-csv.csv', header=TRUE, sep=",", quote="\"")
original <- data.frame(d)
write.csv(original, file='017-diff-weka.csv', row.names = FALSE)
