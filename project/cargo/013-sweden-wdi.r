library(RWeka)
library(discretization)
library(mice)
library(VIM)

this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

d <- read.csv('013-sweden-wdi-data-transpose.csv', header=TRUE, sep=",", quote="\"")
original <- data.frame(d)
year <- original[,1, drop=FALSE]
year <- year[-1,]
difference <- data.frame(diff(as.matrix(original[,2:ncol(original)])))

# write.csv(original, file='013-sweden-wdi-weka-csv.csv', row.names = FALSE)

# print(original)
# missing data
print(md.pattern(original))
aggr_plot <- aggr(difference, col=c('navyblue','red'), numbers=TRUE, sortVars=TRUE, labels=names(difference), cex.axis=.7, gap=3, ylab=c("Histogram of missing data","Pattern"))
# marginplot(original[,c(1,6)])
tempData <- mice(original, m=5,maxit=50,meth='pmm',seed=500)
summary(tempData)

# difference <- disc.Topdown(difference, method = 3)$Disc.data
#
# print(difference)
#
# diff_table <- cbind(year, difference)
# write.csv(diff_table, file='012-sweden-wdi-diff-data.csv', row.names = FALSE)
#
# print(diff_table)
#
# interesting_attrs <- diff_table[, c("Air.transport..freight..million.ton.km.",
#                                     "CO2.emissions.from.transport....of.total.fuel.combustion.",
#                                     "GDP..constant.LCU.",
#                                     "Railways..goods.transported..million.ton.km.")]
# print(interesting_attrs)
#
# print(Discretize(~.,data=interesting_attrs, na.action=na.pass,
#   control=Weka_control(R="2", precision=6)))
