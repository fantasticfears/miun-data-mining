library(RWeka)
library(discretization)
library(mice)
library(VIM)

this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

d <- read.csv('014-sweden.csv', header=TRUE, sep=",", quote="\"")
original <- data.frame(d)
#
# year <- original[,1, drop=FALSE]
# year <- year[-1,]
# difference <- data.frame(diff(as.matrix(original[,2:ncol(original)])))

# write.csv(original, file='013-sweden-wdi-weka-csv.csv', row.names = FALSE)

# print(original)
# missing data
# print(md.pattern(original))
# aggr_plot <- aggr(difference, col=c('navyblue','red'), numbers=TRUE, sortVars=TRUE, labels=names(difference), cex.axis=.7, gap=3, ylab=c("Histogram of missing dataoriginal$Pattern"))
original$Internet.users..per.100.people.[is.na(original$Internet.users..per.100.people.)] <- 0
# d <- data.frame(original$Railways..goods.transported..million.ton.km., original$GDP.growth..annual...)
# d <- data.frame(original$Air.transport..freight..million.ton.km., original$GDP.growth..annual...)
# d <- data.frame(original$Air.transport..passengers.carried, original$GDP.growth..annual...)
# d <- data.frame(original$Air.transport..registered.carrier.departures.worldwide, original$GDP.growth..annual...)
# d <- data.frame(original$Industry..value.added..constant.LCU., original$GDP.growth..annual...)
# d <- data.frame(original$Manufacturing..value.added..constant.LCU., original$GDP.growth..annual...)
# d <- data.frame(original$Rail.lines..total.route.km., original$GDP.growth..annual...)
# d <- data.frame(original$Railways..passengers.carried..million.passenger.km., original$GDP.growth..annual...)
# print(d)
# marginplot(d)

part1 <- data.frame(original$Year, original$Air.transport..freight..million.ton.km.)
part2 <- data.frame(
  original$Merchandise.imports..current.US..,
  original$Air.transport..passengers.carried,
  original$CO2.emissions.from.transport....of.total.fuel.combustion.,
  original$Air.transport..registered.carrier.departures.worldwide)
part3<- data.frame(original$Energy.use..kg.of.oil.equivalent.per.capita.,
  original$Imports.of.goods.and.services..constant.LCU.,
  original$Industry..value.added..constant.LCU.,
  original$Manufacturing..value.added..constant.LCU.,
  original$Railways..goods.transported..million.ton.km.
)
part4 <- data.frame(original$Merchandise.exports..current.US..,
    original$Population.growth..annual..., original$Rail.lines..total.route.km.,
    original$Railways..passengers.carried..million.passenger.km.)
part3 <- data.frame(
  original$Exports.of.goods.and.services....of.GDP.,
original$Agricultural.land....of.land.area., original$GDP.growth..annual..., original$Internet.users..per.100.people.)
tempData1 <- mice(part1, m=5,maxit=50,meth='pmm',seed=500)
tempData2 <- mice(part2, m=5,maxit=50,meth='pmm',seed=500)
tempData3 <- mice(part3, m=5,maxit=50,meth='pmm',seed=500)
# imp1 <- complete(tempData1, 1)
# imp2 <- complete(tempData2, 1)
# imp3 <- complete(tempData3, 1)

# n <- data.frame(original$Year, imp1, imp2, imp3)
# print(completedData <- complete(tempData,1))

# difference <- disc.Topdown(difference, method = 3)$Disc.data
#
# print(difference)
#
# diff_table <- cbind(year, difference)
# write.csv(diff_table, file='012-sweden-wdi-diff-data.csv', row.names = FALSE)
#
# print(diff_table)
#
# interesting_attrs <- diff_table[, c("Air.transport..freight..million.ton.km.",
#                                     "CO2.emissions.from.transport....of.total.fuel.combustion.",
#                                     "GDP..constant.LCU.",
#                                     "Railways..goods.transported..million.ton.km.")]
# print(interesting_attrs)
#
# print(Discretize(~.,data=interesting_attrs, na.action=na.pass,
#   control=Weka_control(R="2", precision=6)))
