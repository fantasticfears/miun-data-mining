import csv

csv_file = './013-sweden-wdi-data-including-gdp-growth.csv'
out = '013-sweden-wdi-data-transpose.csv'

a = zip(*csv.reader(open(csv_file, "r")))
csv.writer(open(out, "w"), quoting=csv.QUOTE_ALL).writerows(a)
