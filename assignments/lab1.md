# Lab 1: Data explain

The original data table names and column names are renamed according to `data-collection.md` file.

## 1. How many tables are in the database?

`SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'football';`

There are 11 tables in the football database.

## 2. Draw a diagram of the relationships in the database. - ER diagram

`ER-diagram.<png, svg>`. It's also stored as `football.uml`.

## 3. For what timespan is the data valid?

Conducted by min max SQL functions. e.g. `SELECT MAX(date) FROM football.games;`

All data is valid except mentioned in detailed.

They are 1000 times (ms) than unix epoch. Represented as `bigint` (datetime):

- `date` in `football.games`:  1059688800000 (2003-07-31T22:00:00+00:00) - 1392667200000 (2014-02-17T20:00:00+00:00). 10 records are invalid and should be ignored due to negative epoch.
- `date` in `football.market_values`: 1072912260000 (2003-12-31T23:11:00+00:00) - 1392332400000 (2014-02-13T23:00:00+00:00).
- `start_contract` in `football.affiliations`: 299631600000 (1979-06-30T23:00:00+00:00) - 1467237600000 (2016-06-29T22:00:00+00:00).
- `end_contract` in `football.affiliations`: 362700000000 (1981-06-29T22:00:00+00:00) - 1498687200000 (2017-06-28T22:00:00+00:00).

Represented as `smallint`:

- `season` in `football.leagues` (year): 2004 - 2014

## 4. What leagues are investigated?

`SELECT name FROM football.leagues GROUP BY name ORDER BY name;`

    1.Bundesliga
    2.Bundesliga
    Championship
    Eredivisie
    Ligue 1
    Ligue 2
    Premier League
    Primeira Liga
    Primera División
    Segunda División
    Segunda Liga
    Serie A
    Serie B

Two leagues are investigated which are `Premier League` and `Eredivisie`.

### `Premier League`

It spans from 2004 to 2014. Its website is [http://www.transfermarkt.de/de/premier-league/startseite/wettbewerb_GB1.html](http://www.transfermarkt.de/de/premier-league/startseite/wettbewerb_GB1.html). It's in England.

It has 6 entries for each year in `table_levels`. They have the same position and values for each year. These data was peculiar as for now.

It has 418 games during 10 years in total. It has 38 games in 2004.

Several SQLs are used:

```sql
SELECT * FROM football.leagues WHERE name = 'Premier League';
SELECT * FROM football.table_levels WHERE league_id <= 55 AND league_id >= 45;
SELECT * FROM football.match_days WHERE league_id <= 55 AND league_id >= 45;
```

### `Eredivisie`

It also spans from 2004 to 2014. Its website is [http://www.transfermarkt.de/de/eredivisie/startseite/wettbewerb_NL1.html](http://www.transfermarkt.de/de/eredivisie/startseite/wettbewerb_NL1.html). It's in Niederlande.

It has 374 games during 10 years in total. It has 34 games in 2014.

It has 18 teams playing in the league in 2014.

In 2014, the teams won 1965 games, lost 1965 games and drawed 1448 games.

Several SQLs are used:

```sql
SELECT * FROM football.leagues WHERE name = 'Eredivisie';
SELECT * FROM football.match_days WHERE league_id <= 121 AND league_id >= 111;
SELECT * FROM football.teams WHERE league_id = 121;
SELECT SUM(t.wins), SUM(t.draws), SUM(t.losses) FROM football.tables t, football.match_days m WHERE m.league_id = 121 AND m.id = t.match_day_id;
```

## 5. What information about players is available?

- Name
- Information url
- Birthday
- Historical affiliations information including contract time (from `affiliations` table)
- Market values (from `market_values` table)

## 6. Why should data analysis never modify the original data but create a copy of it to conduct transformations, joining, filters etc.?

Original datasets is original. All the cumulative transformations changed the original datasets inevitably. When operating on a new copy, the transformations does no harm to the original datasets. It can be easily rollback by deleting the copy. It also does benefit to the new search angles since we can create a new copy based on the original data.

When use new copy we can happily enjoy a performance since the original foreign key and value check can be avoided. It's also easier to exclude the taint data which can be deleted from the copy. In SQL, we can get rid of some conditional check to exclude the taint data. It prevents us from shifting the focus when conduct research and preparation.

## 7. What are attribute type and range for the following database columns:

1. `scores.home`: `<smallint>` 0 - 29
2. `games.score_home`: `<integer>` 0 - 10
3. `players.name`: `<text>` `Aadil Assana` - `홍정호`
4. `match_days.game_of_season`: `<smallint>` 1 - 46

We assumed the closed world assumption here. We believe the data is completed, real and unique. Based on the assumption, we conducted the min max search in the database to get a range from the datasets.

## 8. Create a view that contains a transformation column 'result' with input range {*WIN*, *DRAW*, *LOSS*}, that reflects the result from the perspective of the home team.

```sql
CREATE MATERIALIZED VIEW home_team_games (result) AS
    SELECT 'WIN', g.* FROM football.games g WHERE g.score_home > g.score_away
    UNION ALL
    SELECT 'DRAW', g.* FROM football.games g WHERE g.score_home = g.score_away
    UNION ALL
    SELECT 'LOSS', g.* FROM football.games g WHERE g.score_home < g.score_away;
```

## 9. Find out what a [**materialized view**](http://www.postgresql.org/docs/current/static/sql-creatematerializedview.html) is, and why it might be a valuable feature in the context of data mining.

- Materialized views are concreate, stored the data based on the query definition. Later it can be updated periodically based upon the query definition. The data can be older than a standard view.
- Views are virtual. It's a proxy to query the real tables. It runs the query definition each time they are accessed.

Materialized views are performance-wised in the data mining aspects. The data is rarely updated and finite but the speed is a key which is what materialized view offers.

## 10. Can you identify a potential stakeholder and question for the dataset?

    TODO
### a. What columns (or *attributes*) and which rows (or *instances*) in the database may be of interest for the analysis.


### b. Build a valid **input** in the form that data mining algorithms can work with.
