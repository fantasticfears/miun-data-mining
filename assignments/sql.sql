-- Random inspection
SELECT * FROM leagues WHERE name = 'Premier League';
SELECT * FROM table_levels WHERE league_id <= 55 AND league_id >= 45;
SELECT * FROM match_days WHERE league_id <= 55 AND league_id >= 45;

SELECT * FROM leagues WHERE name = 'Eredivisie';
SELECT * FROM match_days WHERE league_id <= 121 AND league_id >= 111;
SELECT * FROM teams WHERE league_id = 121;
SELECT SUM(t.wins), SUM(t.draws), SUM(t.losses) FROM tables t, match_days m WHERE m.league_id = 121 AND m.id = t.match_day_id;

SELECT * FROM games;

-- Understanding football leagues

SELECT * FROM leagues WHERE season = 2014;
SELECT * FROM teams WHERE league_id = 55;

SELECT COUNT(*), league_id FROM match_days GROUP BY league_id ORDER BY league_id;
SELECT COUNT(t.*), l.name, t.league_id FROM leagues l
JOIN teams t ON t.league_id = l.id AND l.season = 2014
GROUP BY t.league_id, l.name ORDER BY t.league_id;
SELECT COUNT(t), l.name, t.league_id, m.count FROM leagues l
JOIN teams t ON t.league_id = l.id AND l.season = 2014
JOIN (SELECT COUNT(match.*) count, match.league_id FROM match_days match GROUP BY match.league_id) m ON m.league_id = l.id
GROUP BY t.league_id, l.name, m.count ORDER BY t.league_id;

SELECT * FROM match_days m WHERE m.league_id = 55;
SELECT COUNT(*), match_day_id FROM games WHERE match_day_id >= 1971 AND match_day_id <= 2008 GROUP BY match_day_id ORDER BY match_day_id;
SELECT * FROM tables WHERE match_day_id >= 1971 AND match_day_id <= 2008;

SELECT * FROM leagues;
SELECT * FROM table_levels;
SELECT * FROM table_levels WHERE league_id = 55;

SELECT * FROM scores;
SELECT * FROM players
JOIN affiliations ON affiliations.player_id = players.id;

-- player market value comparing to age
SELECT EXTRACT(day FROM (TIMESTAMP WITH TIME ZONE 'epoch' + market_values.date * INTERVAL '1 millisecond' - birthday)) AS age, name, value FROM players
JOIN market_values ON market_values.player_id = players.id
WHERE value > 1000
ORDER BY value DESC ;
SELECT name, value, EXTRACT(year FROM AGE(TIMESTAMP WITH TIME ZONE 'epoch' + market_values.date * INTERVAL '1 millisecond', birthday)) AS age FROM players
JOIN market_values ON market_values.player_id = players.id
WHERE value > 1000
ORDER BY value DESC;
SELECT name, value, EXTRACT(year FROM AGE(TIMESTAMP WITH TIME ZONE 'epoch' + market_values.date * INTERVAL '1 millisecond', birthday)) AS age FROM players
JOIN market_values ON market_values.player_id = players.id
WHERE value > 1000
ORDER BY value DESC;

-- including affiliations
SELECT * FROM players
JOIN affiliations ON players.id = affiliations.player_id
JOIN market_values ON players.id = market_values.player_id;
SELECT name, value, COUNT(a) count,
	EXTRACT(year FROM AGE(TIMESTAMP WITH TIME ZONE 'epoch' + m.date * INTERVAL '1 millisecond', birthday)) AS age
FROM players p
JOIN market_values m ON m.player_id = p.id
JOIN affiliations a ON a.player_id = p.id WHERE a.end_contract IS NULL OR a.end_contract < m.date
GROUP BY a.player_id, p.name, m.value, m.date, p.birthday
HAVING m.value > 1000
ORDER BY name, age, count;

-- lab3
-- Position of home team
SELECT 'HOME' AS home_away, t.league_id, t.name, p.* FROM football.tables p
INNER JOIN football.match_days m ON m.id = p.match_day_id
INNER JOIN football.games g ON g.match_day_id = m.id
INNER JOIN football.teams t ON t.id = p.team_id WHERE g.home_team_id = t.id
ORDER BY match_day_id ASC, position ASC;

-- Position of away team
SELECT 'AWAY' AS home_away, t.league_id, t.name, p.* FROM football.tables p
INNER JOIN football.match_days m ON m.id = p.match_day_id
INNER JOIN football.games g ON g.match_day_id = m.id
INNER JOIN football.teams t ON t.id = p.team_id WHERE g.away_team_id = t.id
ORDER BY match_day_id ASC, position ASC;

-- the difference in position
SELECT p1.position - p2.position AS home_team_difference, t1.name, g1.*, t2.name, m.*
FROM football.games g1
INNER JOIN football.games g2 ON g1.id = g2.id
INNER JOIN football.match_days m ON m.id = g1.match_day_id AND m.id = g2.match_day_id
INNER JOIN football.teams t1 ON t1.id = g1.home_team_id
INNER JOIN football.tables p1 ON p1.match_day_id = m.id AND p1.team_id = t1.id
INNER JOIN football.teams t2 ON t2.id = g2.away_team_id
INNER JOIN football.tables p2 ON p2.match_day_id = m.id AND p2.team_id = t2.id
ORDER BY m.id ASC, g1.id ASC;

-- WIN-DRAW-LOSS
CREATE MATERIALIZED VIEW home_team_games (result) AS
    SELECT 'WIN', g.* FROM football.games g WHERE g.score_home > g.score_away
    UNION ALL
    SELECT 'DRAW', g.* FROM football.games g WHERE g.score_home = g.score_away
    UNION ALL
    SELECT 'LOSS', g.* FROM football.games g WHERE g.score_home < g.score_away;
SELECT * FROM home_team_games; -- As created in lab1

-- lab3 example: 000-home_away_score.csv
SELECT
	home_position,
	away_position,
	position_difference,
	game_of_season,
	CASE WHEN score_home > score_away THEN 'WIN'
       WHEN score_home < score_away THEN 'LOSS'
       ELSE 'DRAW'
	END AS result
	FROM match_days JOIN
		(SELECT
			score_home,
			score_away,
			home_position,
			position AS away_position,
			tables.match_day_id,
			home_position - position AS position_difference
		FROM tables JOIN (
			SELECT
				score_home,
				score_away,
				home_team_id,
				away_team_id,
				temp.match_day_id,
				position AS home_position
			FROM games JOIN (SELECT * FROM tables) temp
			ON temp.team_id = games.home_team_id AND temp.match_day_id = games.match_day_id) home_table
		ON tables.team_id = home_table.away_team_id AND home_table.match_day_id = tables.match_day_id) positions
	ON match_days.id = positions.match_day_id;

SELECT
	home_position,
	away_position,
	position_difference,
	game_of_season,
	score_difference,
	CASE WHEN score_home > score_away THEN 'WIN'
       WHEN score_home < score_away THEN 'LOSS'
       ELSE 'DRAW'
	END AS result
	FROM match_days JOIN
		(SELECT
			score_home,
			score_away,
			home_position,
			position AS away_position,
			tables.match_day_id,
			home_scored - goals_scored AS score_difference,
			home_position - position AS position_difference
		FROM tables JOIN (
			SELECT
				score_home,
				score_away,
				home_team_id,
				away_team_id,
				temp.match_day_id,
				position AS home_position,
				goals_scored AS home_scored
			FROM games JOIN (SELECT * FROM tables) temp
			ON temp.team_id = games.home_team_id AND temp.match_day_id = games.match_day_id) home_table
		ON tables.team_id = home_table.away_team_id AND home_table.match_day_id = tables.match_day_id) positions
	ON match_days.id = positions.match_day_id;
