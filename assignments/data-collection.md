# Preprocess

This preprocess however breaks a rule of Data Mining which is never change the original data. But so sorry. There are a lot of association in the table. Some naming changes does help with liability. Let's why we did this!

## 1. Name normalization

The general principles follow the Rails convention. [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration).

Table names should be lowercases and plural. But column name is singular. Words are connected with underscore `_`. These applies to the column and sequence.

```sql
-- connected with underscore
ALTER SEQUENCE football.matchdays_id_seq RENAME TO match_days_id_seq;

ALTER TABLE football.marketvalues RENAME TO market_values;
ALTER TABLE football.tablelevels RENAME TO table_levels;
ALTER TABLE football.matchdays RENAME TO match_days;

ALTER TABLE football.affiliations RENAME teamname TO team_name;
ALTER TABLE football.affiliations RENAME startcontract TO start_contract;
ALTER TABLE football.affiliations RENAME endcontract TO end_contract;

ALTER TABLE football.games RENAME hometeam TO home_team;
ALTER TABLE football.games RENAME awayteam TO away_team;
ALTER TABLE football.games RENAME scorehome TO score_home;
ALTER TABLE football.games RENAME scoreaway TO score_away;
ALTER TABLE football.games RENAME matchday TO match_day;

ALTER TABLE football.match_days RENAME gameofseason TO game_of_season;

ALTER TABLE football.tables RENAME matchday TO match_day;
ALTER TABLE football.tables RENAME goalsscored TO goals_scored;
ALTER TABLE football.tables RENAME goalsconveived TO goals_conveived;

ALTER TABLE football.table_levels RENAME firstposition TO first_position;
ALTER TABLE football.table_levels RENAME lastposition TO last_position;

-- convert foreign key names
ALTER TABLE football.tables RENAME match_day TO match_day_id;
ALTER TABLE football.tables RENAME team TO team_id;
ALTER TABLE football.teams RENAME league TO league_id;
ALTER TABLE football.table_levels RENAME league TO league_id;
ALTER TABLE football.match_days RENAME league TO league_id;
ALTER TABLE football.games RENAME home_team TO home_team_id;
ALTER TABLE football.games RENAME away_team TO away_team_id;
ALTER TABLE football.games RENAME match_day TO match_day_id;
ALTER TABLE football.market_values RENAME player TO player_id;
ALTER TABLE football.affiliations RENAME player TO player_id;
```

## 2. SQL

Data are lowercases, SQL command or PG functions are uppercases.

## Final solution

```sql
CREATE OR REPLACE VIEW leagues AS
    SELECT * FROM football.leagues;

CREATE OR REPLACE VIEW table_levels (league_id, value, first_position, last_position) AS
    SELECT t.league, t.value, t.firstposition, t.lastposition FROM football.tablelevels t;

CREATE OR REPLACE VIEW match_days (id, game_of_season, league_id) AS
    SELECT t.id, t.gameofseason, t.league FROM football.matchdays t;

CREATE OR REPLACE VIEW teams (id, league_id, url, name) AS
    SELECT t.id, t.league, t.url, t.name FROM football.teams t;

CREATE OR REPLACE VIEW tables (match_day_id, position, team_id, points, wins, draws, losses, goals_scored, goals_conveived) AS
    SELECT t.matchday, t.position, t.team, t.points, t.wins, t.draws, t.losses, t.goalsscored, t.goalsconveived FROM football.tables t;

CREATE OR REPLACE VIEW games (id, home_team_id, away_team_id, score_home, score_away, date, match_day_id) AS
    SELECT t.id, t.hometeam, t.awayteam, t.scorehome, t.scoreaway, t.date, t.matchday FROM football.games t;

CREATE OR REPLACE VIEW players AS
    SELECT * FROM football.players;

CREATE OR REPLACE VIEW market_values (player_id, date, value) AS
    SELECT t.player, t.date, t.value FROM football.marketvalues t;

CREATE OR REPLACE VIEW affiliations (player_id, team_name, start_contract, end_contract) AS
    SELECT t.player, t.teamname, t.startcontract, t.endcontract FROM football.affiliations t;

CREATE OR REPLACE VIEW t AS
    SELECT * FROM football.t;

CREATE OR REPLACE VIEW scores AS
    SELECT * FROM football.scores;
```
