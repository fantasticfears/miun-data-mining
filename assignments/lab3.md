# Lab 3: Model Creation and Validation

The original data table names and column names are renamed according to `data-collection.md` file.

## 2. Soccer Data Analysis

### Data Frame

Using a select statement, create a data-frame that contains the following columns for all games in the database:

* position of home team in league table before the game
* position of away team in league table before the game
* the difference in position between the teams before the game
* *outcome* as nominal attribute from the view of the home team WIN-DRAW-LOSS

SQL used:

```sql
-- Position of home team
SELECT 'HOME' AS home_away, t.league_id, t.name, p.* FROM football.tables p
INNER JOIN football.match_days m ON m.id = p.match_day_id
INNER JOIN football.games g ON g.match_day_id = m.id
INNER JOIN football.teams t ON t.id = p.team_id WHERE g.home_team_id = t.id
ORDER BY match_day_id ASC, position ASC;

-- Position of away team
SELECT 'AWAY' AS home_away, t.league_id, t.name, p.* FROM football.tables p
INNER JOIN football.match_days m ON m.id = p.match_day_id
INNER JOIN football.games g ON g.match_day_id = m.id
INNER JOIN football.teams t ON t.id = p.team_id WHERE g.away_team_id = t.id
ORDER BY match_day_id ASC, position ASC;

-- the difference in position
SELECT p1.position - p2.position AS home_team_difference, t1.name, g1.*, t2.name, m.*
FROM football.games g1
INNER JOIN football.games g2 ON g1.id = g2.id
INNER JOIN football.match_days m ON m.id = g1.match_day_id AND m.id = g2.match_day_id
INNER JOIN football.teams t1 ON t1.id = g1.home_team_id
INNER JOIN football.tables p1 ON p1.match_day_id = m.id AND p1.team_id = t1.id
INNER JOIN football.teams t2 ON t2.id = g2.away_team_id
INNER JOIN football.tables p2 ON p2.match_day_id = m.id AND p2.team_id = t2.id
ORDER BY m.id ASC, g1.id ASC;

-- WIN-DRAW-LOSS
SELECT * FROM home_team_games; -- As created in lab1
```

### Classifier

Assume that you want to build a classifier on the data-set to predict the *outcome*.

* What would be an appropriate way of separating training and test data for the data-set?
* Which is the best classifier you can build for the data?
* How do you validate/evaluate that it is best?
* How important is the use of a cost function in this scenario?

4) Can you extend the data-set with further interesting attributes to reduce the classification error?
